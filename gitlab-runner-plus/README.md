# `docker-machine` (plus)

Image with [Gitlab-forked `docker-machine`](https://gitlab.com/gitlab-org/ci-cd/docker-machine) installed laong with the following docker-machine drivers:

- [x] [`JonasProgrammer/docker-machine-driver-hetzner`](https://github.com/JonasProgrammer/docker-machine-driver-hetzner)
- [ ] [`machine-drivers/docker-machine-driver-qemu`](https://github.com/machine-drivers/docker-machine-driver-qemu)
- [ ] [`machine-drivers/docker-machine-kvm`](https://github.com/machine-drivers/docker-machine-kvm)
- [ ] [`nathanleclaire/docker-machine-driver-dind`](https://github.com/nathanleclaire/docker-machine-driver-dind)
