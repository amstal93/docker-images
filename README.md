# Docker images

Custom images to use with `docker` for various projects

## Images

### `dbench`

Modified version of [`leeliu/dbench`](https://github.com/leeliu/dbench), see `dbench/README.md` for more information.

### KubeVirt images

Various images ([ContainerDisk][kubevirt-containerdisk]s) that can be used with KubeVirt

[kubevirt-containerdisk]: https://kubevirt.io/user-guide/virtual_machines/disks_and_volumes/#containerdisk-workflow-example

### `docker`

A version of [`docker` (specifically `-dind`)](dockerhub-docker) without `/var/lib/docker` specified as a `VOLUME`

[dockerhub-docker]: https://hub.docker.com/_/docker
