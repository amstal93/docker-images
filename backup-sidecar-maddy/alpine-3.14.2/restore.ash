#!/usr/bin/env ash
set -e

# Check provided ENV
if [ -z "$BACKUP_SCRATCH_DIR" ]; then
    BACKUP_SCRATCH_DIR=/tmp
fi

if [ -z "$MADDY_DATA_DIR" ]; then
    MADDY_DATA_DIR=/data
fi

if [ -z "$MADDY_SQLITE_MAIN_DB_FILENAME" ]; then
    MADDY_SQLITE_MAIN_DB_FILENAME=imapsql.db
fi

if [ -z "$MADDY_SQLITE_CREDENTIALS_DB_FILENAME" ]; then
    MADDY_SQLITE_CREDENTIALS_DB_FILENAME=credentials.db
fi

if [ -z "$INTERVAL_SECONDS" ]; then
    INTERVAL_SECONDS=43200
fi

if [ -z "$BUCKET" ]; then
    BUCKET=backups
fi

if [ -z "$BUCKET_PREFIX" ]; then
    BUCKET_PREFIX=maddy
fi

if [ -z "$B2_ACCOUNT_ID" ]; then
    echo -e "B2_ACCOUNT_ID not set"
    exit -1
fi

if [ -z "$B2_KEY" ]; then
    echo -e "B2_KEY not set"
    exit -1
fi

if [ -z "$BACKUP_NAME" ]; then
    echo -e "BACKUP_NAME must be set for a restore"
    exit -1
fi
echo -e "[info] BACKUP_NAME=${BACKUP_NAME}"

export COMPRESSED_BACKUP_NAME=${BACKUP_NAME}.tar.gz
echo -e "[info] COMPRESSED_BACKUP_NAME=${COMPRESSED_BACKUP_NAME}"

echo -e "[info] clearing the scratch directory..."
mkdir -p ${BACKUP_SCRATCH_DIR}
rm -rf ${BACKUP_SCRATCH_DIR}/*

export BACKUP_DIR=${BACKUP_SCRATCH_DIR}/${BACKUP_NAME}
echo -e "[info] BACKUP_DIR=${BACKUP_DIR}"

export BACKUP_FILE_PATH=${BACKUP_SCRATCH_DIR}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] BACKUP_FILE_PATH=${BACKUP_FILE_PATH}"

export REMOTE_BACKUP_DIR=:b2:$BUCKET/${BUCKET_PREFIX}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] retrieving SQLite backup from Backblaze under account [${B2_ACCOUNT_ID}]..."
echo -e "[info] retrieving backup [${REMOTE_BACKUP_DIR}]"
rclone copy \
  --b2-account $B2_ACCOUNT_ID \
  --b2-key $B2_KEY \
  ${REMOTE_BACKUP_DIR} \
  ${BACKUP_SCRATCH_DIR}

# Print backup size
export BACKUP_SIZE=$(du -hs ${BACKUP_FILE_PATH})
echo -e "[info] SQLite Backup size: [${BACKUP_SIZE}]"

# Unzip the backup
echo -e "[info] Unzipping SQLite backup..."
cd ${BACKUP_SCRATCH_DIR} && tar -xf ${COMPRESSED_BACKUP_NAME}

# Restore credentials SQLite DB
export MADDY_SQLITE_CREDENTIALS_DB_PATH=${MADDY_DATA_DIR}/${MADDY_SQLITE_CREDENTIALS_DB_FILENAME}
export MADDY_SQLITE_CREDENTIALS_DB_BACKUP_PATH=${BACKUP_DIR}/${MADDY_SQLITE_CREDENTIALS_DB_FILENAME}
echo "[info] restoring credentials SQLite backup from [${MADDY_SQLITE_CREDENTIALS_DB_BACKUP_PATH}]..."
sqlite3 ${MADDY_SQLITE_CREDENTIALS_DB_PATH} ".restore '${MADDY_SQLITE_CREDENTIALS_DB_BACKUP_PATH}'"
echo -e "[info] credentials SQLite DB backup restored from [${MADDY_SQLITE_CREDENTIALS_DB_BACKUP_PATH}]"

# Restore main SQLite DB
export MADDY_SQLITE_MAIN_DB_PATH=${MADDY_DATA_DIR}/${MADDY_SQLITE_MAIN_DB_FILENAME}
export MADDY_SQLITE_MAIN_DB_BACKUP_PATH=${BACKUP_DIR}/${MADDY_SQLITE_MAIN_DB_FILENAME}
echo "[info] restoring main SQLite backup from [${MADDY_SQLITE_MAIN_DB_PATH}]..."
sqlite3 ${MADDY_SQLITE_MAIN_DB_PATH} ".restore '${MADDY_SQLITE_MAIN_DB_BACKUP_PATH}'"
echo -e "[info] main SQLite DB backup restored from [${MADDY_SQLITE_MAIN_DB_BACKUP_PATH}]"

# Backup ancillary data (messages)
export MESSAGES_DIR=${MADDY_DATA_DIR}/messages
export MESSAGES_BACKUP_DIR=${BACKUP_DIR}/messages
cp -r ${MESSAGES_BACKUP_DIR} ${MESSAGES_DIR}

# Backup ancillary data (DKIM keys)
export DKIM_KEYS_DIR=${MADDY_DATA_DIR}/dkim_keys
export DKIM_KEYS_BACKUP_DIR=${BACKUP_DIR}/dkim_keys
cp -r ${DKIM_KEYS_BACKUP_DIR} ${DKIM_KEYS_DIR}

# Backup ancillary data (maddy configuration)
export MADDY_CONF_PATH=${MADDY_DATA_DIR}/maddy.conf
export MADDY_CONF_BACKUP_PATH=${BACKUP_DIR}/maddy.conf
cp -r ${MADDY_CONF_BACKUP_PATH} ${MADDY_CONF_PATH}

# Backup ancillary data (remote queue)
export MADDY_REMOTE_QUEUE_PATH=${MADDY_DATA_DIR}/remote_queue
export MADDY_REMOTE_QUEUE_BACKUP_PATH=${BACKUP_DIR}/remote_queue
cp -r ${MADDY_REMOTE_QUEUE_BACKUP_PATH} ${MADDY_REMOTE_QUEUE_PATH}

echo "[info] Restore completed from [${REMOTE_BACKUP_DIR}]"
