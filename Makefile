.PHONY: all \
				dbench kubevirt docker gitlab-runner-plus \
				backup-sidecars backup-sidecar-maddy backup-sidecar-listmonk \
				backup-sidecar-ghost backup-sidecar-postgres

all: dbench kubevirt docker backup-sidecars
backup-sidecars: backup-sidecar-maddy backup-sidecar-listmonk backup-sidecar-ghost backup-sidecar-postgres

## Build dbench image
dbench:
	$(MAKE) -s -C dbench

## Kubevirt images
kubevirt:
	$(MAKE) -s -C kubevirt

## docker
docker:
	$(MAKE) -s -C docker

## gitlab-runner-plus
gitlab-runner-plus:
	$(MAKE) -s -C gitlab-runner-plus

## backup-sidecar-maddy
backup-sidecar-maddy:
	$(MAKE) -s -C backup-sidecar-maddy

## backup-sidecar-listmonk
backup-sidecar-listmonk:
	$(MAKE) -s -C backup-sidecar-listmonk

## backup-sidecar-postgres
backup-sidecar-postgres:
	$(MAKE) -s -C backup-sidecar-postgres

## backup-sidecar-ghost
backup-sidecar-ghost:
	$(MAKE) -s -C backup-sidecar-ghost
