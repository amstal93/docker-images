# `dind`

`dind` image with no `VOLUME` specified to get around issues with `kata-containers` (avoiding `kataShared` designation):

- https://github.com/kata-containers/runtime/issues/2493
- https://github.com/kata-containers/runtime/issues/1888#issuecomment-739057384
- https://github.com/kata-containers/runtime/issues/1429#issuecomment-477385283
