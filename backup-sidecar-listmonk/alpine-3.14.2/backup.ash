#!/usr/bin/env ash
set -e

# Check provided ENV
if [ -z "$BACKUP_SCRATCH_DIR" ]; then
    BACKUP_SCRATCH_DIR=/tmp
fi

if [ -z "$LISTMONK_DATA_DIR" ]; then
    LISTMONK_DATA_DIR=/data
fi

if [ -z "$INTERVAL_SECONDS" ]; then
    INTERVAL_SECONDS=43200
fi

if [ -z "$BUCKET" ]; then
    BUCKET=backups
fi

if [ -z "$BUCKET_PREFIX" ]; then
    BUCKET_PREFIX=listmonk
fi

if [ -z "$PGHOST" ]; then
    echo -e "PGHOST not set"
    exit -1
fi

if [ -z "$PGUSER" ]; then
    echo -e "PGUSER not set"
    exit -1
fi

if [ -z "$PGPASSWORD" ]; then
    echo -e "PGPASSWORD not set"
    exit -1
fi

if [ -z "$B2_ACCOUNT_ID" ]; then
    echo -e "B2_ACCOUNT_ID not set"
    exit -1
fi

if [ -z "$B2_KEY" ]; then
    echo -e "B2_KEY not set"
    exit -1
fi

if [ -z "$BACKUP_NAME_PREFIX" ]; then
    BACKUP_NAME_PREFIX=backup
fi

BACKUP_NAME=${BACKUP_NAME_PREFIX}-`date +%F@%H_%M_%S-%Z`
echo -e "[info] BACKUP_NAME=${BACKUP_NAME}"

export BACKUP_DIR=${BACKUP_SCRATCH_DIR}/${BACKUP_NAME}
echo -e "[info] BACKUP_DIR=${BACKUP_DIR}"

# Create backup dir if not already present
mkdir -p ${BACKUP_DIR}

# Backup listmonk postgres instance
# The postgres instance is expected to be accessible without credentials locally
# For remote/locally secured postgres instances, PGUSER/PGHOST/PGPASSWORD/etc must be set
export LISTMONK_PG_BACKUP_PATH=${BACKUP_DIR}/postgres.dumpall.sql
export LISTMONK_PG_COMPRESSED_BACKUP_PATH=${LISTMONK_PG_BACKUP_PATH}.gz
echo "[info] saving Postgres pg_dumpall backup @ [${LISTMONK_PG_MAIN_DB_PATH}]..."
pg_dumpall --clean --if-exists --file=${LISTMONK_PG_BACKUP_PATH}
echo -e "[info] postgres backup taken, stored @ [${LISTMONK_PG_BACKUP_PATH}]"
gzip ${LISTMONK_PG_BACKUP_PATH}
echo -e "[info] postgres backup compressed, stored @ [${LISTMONK_PG_COMPRESSED_BACKUP_PATH}]"

# Backup ancillary main data container folder
export DATA_DIR=${LISTMONK_DATA_DIR}
export DATA_BACKUP_DIR=${BACKUP_DIR}/data
cp -r ${DATA_DIR} ${DATA_BACKUP_DIR}

# Print backup size
export BACKUP_SIZE=$(du -hs ${BACKUP_DIR})
echo -e "[info] Total backup size: [${BACKUP_SIZE}]"

# Build the name of the compressed backup
export COMPRESSED_BACKUP_NAME=${BACKUP_NAME}.tar.gz
echo -e "[info] COMPRESSED_BACKUP_NAME=${COMPRESSED_BACKUP_NAME}"

echo -e "[info] Zipping complete backup..."
cd ${BACKUP_SCRATCH_DIR} && tar -czf ${COMPRESSED_BACKUP_NAME} ${BACKUP_NAME}

# Build the expected path of the compressed backup
export COMPRESSED_BACKUP_FILE_PATH=${BACKUP_SCRATCH_DIR}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] COMPRESSED_BACKUP_FILE_PATH=${COMPRESSED_BACKUP_FILE_PATH}"

# Upload the backup to B2
export REMOTE_BACKUP_DIR=:b2:$BUCKET/${BUCKET_PREFIX}
echo -e "[info] Saving backup to Backblaze under account [${B2_ACCOUNT_ID}]..."
echo -e "[info] Saving backup to [${REMOTE_BACKUP_DIR}]"
rclone copy \
  --b2-account $B2_ACCOUNT_ID \
  --b2-key $B2_KEY \
  ${COMPRESSED_BACKUP_FILE_PATH} \
  ${REMOTE_BACKUP_DIR}

echo "[info] Backup completed, saved to [${REMOTE_BACKUP_DIR}/${COMPRESSED_BACKUP_NAME}]"
