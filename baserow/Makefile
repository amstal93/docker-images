.PHONY: all image publish \
# backend
				backend image-backend publish-backend \
# frontend-web
				frontend-web image-frontend-web publish-frontend-web \
# media
				media image-media publish-media

DOCKER ?= docker
VERSION ?= 1.10.0

REGISTRY ?= registry.gitlab.com/mrman/docker-images/baserow

all: image publish

image: image-backend image-frontend-web image-media

publish: publish-backend publish-frontend-web publish-media

###########
# Backend #
###########

BACKEND_IMAGE_NAME ?= backend
BACKEND_IMAGE_NAME_FULL ?= $(BACKEND_IMAGE_NAME):$(VERSION)
BACKEND_REGISTRY_IMAGE_NAME_FULL = $(REGISTRY)/$(BACKEND_IMAGE_NAME_FULL)

image-backend:
	$(DOCKER) build \
		--file $(VERSION)/backend/Dockerfile \
		--tag $(BACKEND_IMAGE_NAME_FULL) \
		--tag $(BACKEND_REGISTRY_IMAGE_NAME_FULL) \
		.
publish-backend:
	$(DOCKER) push '$(BACKEND_REGISTRY_IMAGE_NAME_FULL)'

############
# Frontend #
############

FRONTEND_WEB_IMAGE_NAME ?= frontend-web
FRONTEND_WEB_IMAGE_NAME_FULL ?= $(FRONTEND_WEB_IMAGE_NAME):$(VERSION)
FRONTEND_WEB_REGISTRY_IMAGE_NAME_FULL = $(REGISTRY)/$(FRONTEND_WEB_IMAGE_NAME_FULL)

image-frontend-web:
	$(DOCKER) build \
		--file $(VERSION)/frontend-web/Dockerfile \
		--tag $(FRONTEND_WEB_IMAGE_NAME_FULL) \
		--tag $(FRONTEND_WEB_REGISTRY_IMAGE_NAME_FULL) \
		.

publish-frontend-web:
	$(DOCKER) push '$(FRONTEND_WEB_REGISTRY_IMAGE_NAME_FULL)'

#########
# Media #
#########

MEDIA_IMAGE_NAME ?= media
MEDIA_IMAGE_NAME_FULL ?= $(MEDIA_IMAGE_NAME):$(VERSION)
MEDIA_REGISTRY_IMAGE_NAME_FULL = $(REGISTRY)/$(MEDIA_IMAGE_NAME_FULL)

image-media:
	$(DOCKER) build \
		--file $(VERSION)/media/Dockerfile \
		--tag $(MEDIA_IMAGE_NAME_FULL) \
		--tag $(MEDIA_REGISTRY_IMAGE_NAME_FULL) \
		.
publish-media:
	$(DOCKER) push '$(MEDIA_REGISTRY_IMAGE_NAME_FULL)'
