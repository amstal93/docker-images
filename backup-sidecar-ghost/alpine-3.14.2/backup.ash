#!/usr/bin/env ash
set -e

# Check provided ENV
if [ -z "$BACKUP_SCRATCH_DIR" ]; then
    BACKUP_SCRATCH_DIR=/tmp
fi

if [ -z "$GHOST_DATA_DIR" ]; then
    GHOST_DATA_DIR=/var/lib/ghost/content
fi

if [ -z "$GHOST_SQLITE_MAIN_DB_FILENAME" ]; then
    GHOST_SQLITE_MAIN_DB_FILENAME=ghost.db
fi

if [ -z "$INTERVAL_SECONDS" ]; then
    INTERVAL_SECONDS=43200
fi

if [ -z "$BUCKET" ]; then
    BUCKET=backups
fi

if [ -z "$BUCKET_PREFIX" ]; then
    BUCKET_PREFIX=ghost
fi

if [ -z "$B2_ACCOUNT_ID" ]; then
    echo -e "B2_ACCOUNT_ID not set"
    exit -1
fi

if [ -z "$B2_KEY" ]; then
    echo -e "B2_KEY not set"
    exit -1
fi

if [ -z "$BACKUP_NAME_PREFIX" ]; then
    BACKUP_NAME_PREFIX=backup
fi

BACKUP_NAME=${BACKUP_NAME_PREFIX}-`date +%F@%H_%M_%S-%Z`
echo -e "[info] BACKUP_NAME=${BACKUP_NAME}"

export COMPRESSED_BACKUP_NAME=${BACKUP_NAME}.tar.gz
echo -e "[info] COMPRESSED_BACKUP_NAME=${COMPRESSED_BACKUP_NAME}"

export BACKUP_DIR=${BACKUP_SCRATCH_DIR}/${BACKUP_NAME}
echo -e "[info] BACKUP_DIR=${BACKUP_DIR}"

export BACKUP_FILE_PATH=${BACKUP_SCRATCH_DIR}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] BACKUP_FILE_PATH=${BACKUP_FILE_PATH}"

# Create backup dir
mkdir -p ${BACKUP_DIR}

# Backup main SQLite DB
export GHOST_SQLITE_MAIN_DB_PATH=${GHOST_DATA_DIR}/data/${GHOST_SQLITE_MAIN_DB_FILENAME}
export GHOST_SQLITE_MAIN_DB_BACKUP_PATH=${BACKUP_DIR}/${GHOST_SQLITE_MAIN_DB_FILENAME}
echo "[info] taking SQLite backup from file @ [${GHOST_SQLITE_MAIN_DB_PATH}]..."
sqlite3 ${GHOST_SQLITE_MAIN_DB_PATH} ".backup '${GHOST_SQLITE_MAIN_DB_BACKUP_PATH}'"
echo -e "[info] main SQLite DB backup taken, @ [${GHOST_SQLITE_MAIN_DB_BACKUP_PATH}]"

# Backup ancillary data (apps)
export APPS_DIR=${GHOST_DATA_DIR}/apps
export APPS_BACKUP_DIR=${BACKUP_DIR}/apps
cp -r ${APPS_DIR} ${APPS_BACKUP_DIR}

# Backup ancillary data (images)
export IMAGES_DIR=${GHOST_DATA_DIR}/images
export IMAGES_BACKUP_DIR=${BACKUP_DIR}/images
cp -r ${IMAGES_DIR} ${IMAGES_BACKUP_DIR}

# Backup ancillary data (logs)
export LOGS_DIR=${GHOST_DATA_DIR}/logs
export LOGS_BACKUP_DIR=${BACKUP_DIR}/logs
cp -r ${LOGS_DIR} ${LOGS_BACKUP_DIR}

# Backup ancillary data (themes)
export THEMES_DIR=${GHOST_DATA_DIR}/themes
export THEMES_BACKUP_DIR=${BACKUP_DIR}/themes
cp -r ${THEMES_DIR} ${THEMES_BACKUP_DIR}

# Backup ancillary data (settings)
export SETTINGS_DIR=${GHOST_DATA_DIR}/settings
export SETTINGS_BACKUP_DIR=${BACKUP_DIR}/settings
cp -r ${SETTINGS_DIR} ${SETTINGS_BACKUP_DIR}

# Print backup size
export BACKUP_SIZE=$(du -hs ${BACKUP_DIR})
echo -e "[info] SQLite Backup size: [${BACKUP_SIZE}]"

echo -e "[info] Zipping SQLite backup..."
cd ${BACKUP_SCRATCH_DIR} && tar -czf ${COMPRESSED_BACKUP_NAME} ${BACKUP_NAME}

export REMOTE_BACKUP_DIR=:b2:$BUCKET/${BUCKET_PREFIX}
echo -e "[info] saving SQLite backup to Backblaze under account [${B2_ACCOUNT_ID}]..."
echo -e "[info] saving backup to [${REMOTE_BACKUP_DIR}]"
rclone copy \
  --b2-account $B2_ACCOUNT_ID \
  --b2-key $B2_KEY \
  ${BACKUP_FILE_PATH} \
  ${REMOTE_BACKUP_DIR}

echo "[info] Backup completed, saved to [${REMOTE_BACKUP_DIR}/${COMPRESSED_BACKUP_NAME}]"
